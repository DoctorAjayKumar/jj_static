% @doc
% Pure module that scans an HTTP directory and makes a list of
% records for an ETS table.
% @end
-module(jjs_http_dir).

-export([
    dir/1
]).

-record(http_resource,
        {http_path     :: binary(),
         contents      :: binary(),
         last_modified :: calendar:datetime(),
         content_type  :: string()}).

-type http_resource()           :: #http_resource{}.
-type http_resources_deeplist() :: list(http_resource() | http_resources_deeplist()).
-type faggy_timestamp()         :: genesis
                                 | {ts, calendar:datetime()}.

%%% API

-spec dir(RootDirectoryName) -> Result
    when RootDirectoryName :: string(),
         Result            :: {ok, http_resources_deeplist()}
                            | {error, Reason},
         Reason            :: term().

dir(RootDirectoryName) ->
    DeepList = dir3(RootDirectoryName, [], genesis),
    DeepList.



%%% Internals

-spec dir3(RootDirectoryName, CurrentSubDirectoryParts, IgnoreIfLastModifiedLEQ) -> Result
    when RootDirectoryName        :: string(),
         CurrentSubDirectoryParts :: [string()],
         IgnoreIfLastModifiedLEQ  :: faggy_timestamp(),
         Result                   :: {ok, http_resources_deeplist()}
                                   | {error, Reason},
         Reason                   :: term().
%% @private
%% Search through a directory recursively and produce a deeplist of
%% resources.
%% @end

dir3(Root, CurrentSubDirectoryParts, genesis) ->
    CurrentWorkingDirectory = filename:join([Root] ++ CurrentSubDirectoryParts),
    {ok, FileNames} = file:list_dir(CurrentWorkingDirectory),
    dir5(Root, CurrentSubDirectoryParts, genesis, FileNames, []).



-spec dir5(RootDirectoryName, CurrentSubDirectoryParts, IgnoreIfLastModifiedLEQ, FileNames, Accum) -> Result
    when RootDirectoryName        :: string(),
         CurrentSubDirectoryParts :: [string()],
         IgnoreIfLastModifiedLEQ  :: faggy_timestamp(),
         RemainingFileNames       :: [string()],
         Accum                    :: http_resources_deeplist().
         Result                   :: {ok, http_resources_deeplist()}
                                   | {error, Reason},
         Reason                   :: term().
%% @private
%% Search through a directory recursively and produce a deeplist of
%% resources.
%% @end

dir5(_Root, _CurrentSubDirectoryParts, _IgnoreIfLastModifiedLEQ, _RemainingFileNames = [], HttpResourcesDeeplist_FinalAccum) ->
    HttpResourcesDeeplist_FinalAccum;
dir5(Root, CurrentSubDirectoryParts, IgnoreIfLastModifiedLEQ, _RemainingFileNames = [FileName | Rest], HttpResourcesDeeplist_Accum) ->
    FilesystemPath = filename:join([Root] ++ CurrentSubDirectoryParts ++ [FileName]),
    {ok, FileInfo} = file:read_file_info(FilesystemPath, [{time, universal}]),
    {file_info, _Size, FileType, _Access, _ATime, LastModifiedTime, _CTime, _Mode, _Links, _MajorDevice, _MinorDevice, _INode, _UID, _GID} = FileInfo,
    file(Root,
         CurrentSubDirectoryParts,
         IgnoreIfLastModifiedLEQ,
         Rest,
         Accum,
         FileType,
         LastModifiedTime).



-spec file(RootDirectoryName,
           CurrentSubDirectoryParts,
           IgnoreIfLastModifiedLEQ,
           CurrentWorkingDirectoryRemainingFileNames,
           HttpResourcesDeeplist_Accum,
           CurrentFile_FilesystemPath,
           CurrentFile_Name,
           CurrentFile_Type,
           LastModifiedTime
           ) -> Result
    when RootDirectoryName        :: string(),
         CurrentSubDirectoryParts :: [string()],
         IgnoreIfLastModifiedLEQ  :: faggy_timestamp(),
         RemainingFileNames       :: [string()],
         Accum                    :: http_resources_deeplist().
         Result                   :: {ok, http_resources_deeplist()}
                                   | {error, Reason},
         Reason                   :: term().
file(
