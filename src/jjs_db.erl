% @doc
% This needs to
% - traverse a directory
% - make an index of file contents
% - for each `foo/index.html` also add a `foo/` route and a `foo` route
% - for each /index.html also add a / route and a foo route
% - ah, jj_static needs a handler
% - because some of these return redirects
-module(jjs_db).

%-behavior(gen_server).
%
%% API: startup
%-export([
%    start_link/0
%]).
%% gen server exports
%-export([
%    init/1,
%    handle_call/3,
%    handle_cast/2,
%    handle_info/2
%]).
%
%
%-spec start_link() -> Result
%    when Result :: {ok, pid()}
%                 | {error, Reason :: term()}.
%
%start_link() ->
%    gen_server:start_link(
