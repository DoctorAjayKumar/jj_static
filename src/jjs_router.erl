% @doc
% The router
% @end
-module(jjs_router).

%-behavior(oh_router).

-type method()  :: 'GET' | 'POST'.
-type uri()     :: string().
-type handler() :: atom().

-spec can_you_handle_this(method(), uri()) -> {yes, handler()} | no.

can_you_handle_this(_, _) ->
    {yes, jjs_handler}.
