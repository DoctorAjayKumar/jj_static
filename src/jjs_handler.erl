-module(jjs_handler).

%-behavior(oh_handler).
%-behavior(oh_handler).

-include("$oh_include/oh.hrl").

-spec handle(request()) -> response().

handle(_) ->
    #rsp{content = content()}.



-spec content() -> binary().

content() ->
    unicode:characters_to_binary(content_chars()).



-spec content_chars() -> iolist().

content_chars() ->
    ["<html>",
        "<head>",
            "<title>Hello, joggers!</title>",
        "</head>",
        "<body>",
            "<h1>",
                "hello joggers!",
            "</h1>",
        "</body>",
     "</html>"]
